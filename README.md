How to use the Bag class:

1. Initialize the org.happy.artist.rmdmia.utilities.ros.bag.Bag class.
2. Call bag.start(OutputStream os, Bag.CHUNK_COMPRESSION_NONE)); // Where os is the OutputStream you intend to write the file. Examples could be a FileOutputStream, or a network output stream that writes the file to Google Drive, or Dropbox (just examples).
3.  Call bag.addConnectionHeader(char[] topic, int conn, char[] connection_header_hex); for each new connection header on connection handshake. int conn is a unique int connection id chosen for the connection (might be a good idea to iterate through topic ids to create an int array, or other mechanism to chose a unique int. connection_header_hex will be the ROS Serialized Message in the connection header. 
4. Call bag.addMessage(long time, int conn, char[] message_data_hex); Pass in the long time, associated connection header int conn id, and the ROS serialized message to add to the rosbag file.

This Java code is poorly documented, however, I wanted to share it with ROS Community for Java/Android ROS clients that want to record rosbag files. Good luck using it. Releasing under Apache 2.0 license.